/** James Gavin- 05/21/2023
	HII Internship Enlighten Programming Challenge
	
	PagingEntry- stores all information taken in from the satellite's readings
 	*/

import java.text.DecimalFormat;
import java.util.ArrayList;


public class PagingEntry {
	
	// time measurements
	private int year;
	private int month;
	private int day;
	private int hour;
	private int minute;
	private double second;
	private double time_in_seconds;
	// satellite id
	private int id;
	// sensor values
	private int red_high_limit;
	private int yellow_high_limit;
	private int yellow_low_limit;
	private int red_low_limit;
	// measured value
	private double raw_value; 
	private String component;
	
	
	public PagingEntry(String rawEntryData, int lineNum) {
		String[] parsedData = rawEntryData.split("\\|");
		/* Ensure this line of data has the proper amount of arguments */
		if (parsedData.length != 8) {
			System.out.println("\n***** ERROR IN FILE ***** ");
			System.out.println("Line number " + lineNum + " does NOT have the correct number of arguments!");
			/* Save entry as invalid */
			id = -1;
			component = "INVALID";
		} else {
			// Parse through the meta data containing the date and time
			String metaDate[] = parsedData[0].split(" "); 
			// obtain the year, month, and day from metaDate string
			year = Integer.parseInt(metaDate[0].substring(0, 4));
			month = Integer.parseInt(metaDate[0].substring(4, 6));
			day = Integer.parseInt(metaDate[0].substring(6, 8));
			// parse second string in metaDate to get the hour, minute, and seconds
			String timeData[] = metaDate[1].split(":");
			hour = Integer.parseInt(timeData[0]);
			minute = Integer.parseInt(timeData[1]);
			second = Double.parseDouble(timeData[2]);
			// get the time in seconds for sorting entries/quick calculations
			time_in_seconds = second + minute * 60 + hour * 3600;
			// collect the satellite's id, high/low limits, raw values, and component from parsed data
			id = Integer.parseInt(parsedData[1]);
			red_high_limit = Integer.parseInt(parsedData[2]);
			yellow_high_limit = Integer.parseInt(parsedData[3]);
			yellow_low_limit = Integer.parseInt(parsedData[4]);
			red_low_limit = Integer.parseInt(parsedData[5]);
			raw_value = Double.parseDouble(parsedData[6]);
			component = parsedData[7];
		}
	}
	
	
	// Gets this entries log id
	public int getID() {
		return id;
	}
	
	
	// Get this entry's time in seconds
	public double getTime() {
		return time_in_seconds;
	}
	
	
	// Gets the formatted timeStamp for this entry
	public String getTimeStamp() {
		DecimalFormat i = new DecimalFormat("00");
		DecimalFormat d = new DecimalFormat("00.000");
		String timeStamp = year + "-" + i.format(month) + "-" + i.format(day) + "T" + i.format(hour) 
				+ ":" + i.format(minute) + ":" + d.format(second) + "Z";
		return timeStamp;
	}
	
	
	// Gets this entry's hour time
	public int getHour() {
		return hour;
	}
	
	
	// Gets this entry's hour minute
	public int getMinute() {
		return minute;
	}
	
	
	// Gets this entry's hour second
	public double getSecond() {
		return second;
	}
	
	
	// Gets this entry's component that is being measured
	public String getComponent() {
		return component;
	}
	
	
	// check if thermostat readings exceed the red high limit
	public boolean isThermoAlarm() {
		return raw_value > (double)red_high_limit;
	}
	
	
	// check if battery readings are below the red low limit
	public boolean isBatteryAlarm() {
		return raw_value < (double)red_low_limit;
	}
	
	
	
	
}