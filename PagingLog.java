/** James Gavin- 05/21/2023
	HII Internship Enlighten Programming Challenge
	
	PagingLog- stores an array containing all thermostat readings and 
	battery voltage readings for each satellite.
 	*/

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;


public class PagingLog {
	
	// stores an ArrayList for each satellite containing each PageEntry associated with the satellite
	private ArrayList<ArrayList<PagingEntry>> systemLog = new ArrayList<ArrayList<PagingEntry>>();
	// constant for the number of seconds in 5 minutes
	public final int FIVE_MINS = 300;
	
	
	public PagingLog(Scanner sc) {
		String line = "";
		int num = 0;
		/* Go through each telemetry data line and create a new PagingEntry */
		while(sc.hasNextLine()) {
			line = sc.nextLine();
			num++;
			PagingEntry thisEntry = new PagingEntry(line, num); 
			/* Store the PagingEntry in its corresponding log */
			int logIndex = findLog(thisEntry.getID());
			if (logIndex == -1) {
				ArrayList<PagingEntry> newLog = new ArrayList<PagingEntry>();
				newLog.add(thisEntry);
				systemLog.add(newLog);
			} else {
				systemLog.get(logIndex).add(thisEntry);
			}
		}
		// sort each satellites entries by timestamp in ascending order
		for (ArrayList<PagingEntry> thisLog : systemLog) {
			Collections.sort(thisLog, new Comparator<PagingEntry>() {
				@Override
				public int compare(PagingEntry e1, PagingEntry e2) {
					return Double.compare(e1.getTime(), e2.getTime());
				}
			});
		}
	}
	
	
	// determines if this satellite ID is found within the system log
	// if not, returns -1
	private int findLog(int id) {
		for (int i = 0; i < systemLog.size(); i++)
			if (systemLog.get(i).get(0).getID() == id)
				return i;
		return -1;
	}
	
	
	public JsonArray detectAlerts() throws JsonException {
		// logs each alert into a JsonArray
		JsonArrayBuilder jsonAlertLog = Json.createArrayBuilder();
		// for each satellites log in the system log, go through and detect if there are
		// any system alerts (3 alarms all within a 5 minute span)
		for (ArrayList<PagingEntry> thisLog : systemLog) {
			// go through each entry in this log; if it is an alarm, check for 3 more errors within 
			// the next 5 minutes, and if so, write the JSON file that an alert happened at the earliest time stamp
			for (int i = 0; i < thisLog.size(); i++) {
				PagingEntry thisEntry = thisLog.get(i);
				if (thisEntry.isThermoAlarm() || thisEntry.isBatteryAlarm()) {
					// if we detect an alert (3 alarms in 5 minutes for the same component), then create a JsonObject 
					// and add it to our JsonArray
					if (alarmCheck(thisLog, i)) {
						 JsonObjectBuilder newEntry = Json.createObjectBuilder();
						 newEntry.add("satelliteId", String.valueOf(thisEntry.getID()));
						 if (thisEntry.getComponent().equals("TSTAT"))
							 newEntry.add("severity", "RED HIGH");
						 else
							 newEntry.add("severity", "RED LOW");
						 newEntry.add("component", thisEntry.getComponent());
						 newEntry.add("timestamp", thisEntry.getTimeStamp());
						 JsonObject newEntryFinal = newEntry.build();
						 jsonAlertLog.add(newEntryFinal); 
					}
				}
			}
		}
		JsonArray jsonAlertLogFinal = jsonAlertLog.build();
		return jsonAlertLogFinal;
	}
	
	
	private boolean alarmCheck(ArrayList<PagingEntry> thisLog, int startIndex) {
		// alarm count, starts at one since we one had been previously detected
		int cnt = 1;
		// get time first alarm was detected and which alarm it was
		double startTime = thisLog.get(startIndex).getTime();
		String alarmType = thisLog.get(startIndex).isThermoAlarm() ? "TSTAT" : "BATT";
		int index = startIndex + 1;
		// evaluate each subsequent entry as long as we are within the start time + 5 mins
		while (index < thisLog.size() && thisLog.get(index).getTime() < startTime + FIVE_MINS) {
			// check if there was an alarm if this entry has the same reading type, increase alarm count
			if (alarmType.equals(thisLog.get(index).getComponent())) {
				if (alarmType.equals("BATT") && thisLog.get(index).isBatteryAlarm()) {
					cnt++;
				} else if (alarmType.equals("TSTAT") && thisLog.get(index).isThermoAlarm()) {
					cnt++;
				}
			}
			if (cnt == 3)		// check if we have encountered 3 alarms, if so return true
				return true;
			index++;	
		}
		return false;
	}
}
