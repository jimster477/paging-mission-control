/** James Gavin- 05/21/2023
	HII Internship Enlighten Programming Challenge
	
	PaginingController- Main project file; reads in input telemetry data, 
	creates a PagingLog for the entry data, and then runs diagnostic test for
	thermostat and battery voltage readings exceeding their limits.
*/

import java.io.File;
import java.io.FileNotFoundException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonException;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;


public class PagingController {
	
	/* Obtains a scanner to read in the file with the provided name. 
	 * Must be in the directory where the file resides */
	private static Scanner getFileScanner(String fileName) {
        Scanner sc = null;
        try {
        	/* Scanner properly obtained */
            sc = new Scanner(new File(fileName));
        } catch (FileNotFoundException e) {
        	/* If scanner for the file cannot be obtained, print out error info */
            System.out.println("\n***** ERROR IN READING FILE ***** ");
            System.out.println("Cannot find this file " 
                    + fileName + " in the current directory.");
            System.out.println("Error: " + e);
            String currentDir = System.getProperty("user.dir");
            System.out.println("Be sure " + fileName + " is in this directory: ");
            System.out.println(currentDir);
            System.out.println("\nReturning null from method.");
        }
        return sc;
    }
	
	
	public static void main (String[] args) throws JsonException {
		/* File storing the sample telemetry input log */
		final String inputFile = "sampleData.txt";
		// obtain a scanner and the read in the sampleData
		Scanner fileScanner = getFileScanner(inputFile);
		PagingLog systemLog = new PagingLog(fileScanner);
		// detects all RED HIGH/LOW alerts
		JsonArray jsonAlertLog = (JsonArray) systemLog.detectAlerts();
		
		// the following formats the jsonArray
		Map<String, Boolean> writerConfig = new HashMap<>();
        writerConfig.put(JsonGenerator.PRETTY_PRINTING, true);
        // string writer holds the formatted JSON
        StringWriter stringWriter = new StringWriter();
        // write JsonArray to StringWriter with formatting
        JsonWriterFactory writerFactory = Json.createWriterFactory(writerConfig);
        JsonWriter jsonWriter = writerFactory.createWriter(stringWriter);
        jsonWriter.writeArray(jsonAlertLog);
        jsonWriter.close();
        // print formatted JSON to console
        System.out.println(stringWriter.toString());
	}
}